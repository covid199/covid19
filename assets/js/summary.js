// RESET Local Storage
const logout = () => {
	localStorage.clear()
}

document.querySelector('#reTest').addEventListener('click', () => {
	logout()
})
// END RESET

// Accordion
let acc = document.getElementsByClassName("accordion")
let i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the active panel */
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}


//show pui or pum or notpuipum
let final = localStorage.getItem('final');
console.log(final);
if (final === 'pui') {
  document.querySelector("#pui").innerHTML = `<h5>You are categorized as PUI (Person Under Investigation).</h5><p>Please seek medical assistance immediately.</p>`
  document.querySelector("#pui").style.visibility = "visible";
}
else if (final === 'pum' ) {
  document.querySelector("#pum").innerHTML = `<h5>You are categorized as PUM (Person Under Monitoring). Self-Isolate</h5><p>You should stay home and away from others. If you can, have a room and bathroom that’s just for you. This can be hard when you’re not feeling well, but it will protect those around you.</p>`
  document.querySelector("#pum").style.visibility = "visible";
}
else{
  document.querySelector("#npuipum").innerHTML= `<h5>You are neither PUI nor PUM. Stay healthy. Observe Social Distancing</h5><p>Help stop the spread. When outside the home, stay at least six feet away from other people, avoid groups, and only use public transit if necessary.</p>`;
  document.querySelector("#npuipum").style.visibility = "visible";
}

// patientSymptomsResult
const patientRow = [
  document.querySelector("#nonA"),
  document.querySelector("#nonB"),
  document.querySelector("#nonC"),
  document.querySelector("#nonD"),
  ];
let patients = localStorage.getItem('patientSymptoms');
  if (patients !== null) {

    patients = patients.split(',');
    let x;
    for (x = 0; x < patients.length; x++){
      patientRow[x].innerHTML = `<td> ${patients[x]} </td><td>YES</td>`;
    }
  }

//notTravellerResult
let nTravYes = localStorage.getItem('nTravYes');
let nTravYesQ = localStorage.getItem('nTravYesQ')
if (nTravYes !==null && nTravYesQ !== null) {

  document.querySelector("#nonE").innerHTML = `<td> ${nTravYesQ} </td><td>${nTravYes}</td>`;
}
  
//occurenceResult
let occyes = localStorage.getItem('occyes');
let occyesQ = localStorage.getItem('occyesQ')
if (occyes !== null && occyesQ !== null) {

  document.querySelector("#nonF").innerHTML = `<td> ${occyesQ} </td><td>${occyes}</td>`;
}

//closeContactResult
const closeContactRow = [
  document.querySelector("#nonG"),
  document.querySelector("#nonH"),
  document.querySelector("#nonI"),
  document.querySelector("#nonJ"),
  ];
let closeContact = localStorage.getItem('closeContact');
if (closeContact !== null) {
  closeContact = closeContact.split(',');
  let y;
  for (y = 0; y < closeContact.length; y++){
    closeContactRow[y].innerHTML = `<td> ${closeContact[y]} </td><td>YES</td>`;
  }
}  
 


//respiratoryProblemResult
const respiratoryProblemRow = [
  document.querySelector("#nonK"),
  document.querySelector("#nonL"),
  document.querySelector("#nonM"),
  document.querySelector("#nonN"),
  ];
let respiratoryProblem = localStorage.getItem('respiratoryProblem');
if (respiratoryProblem !== null) {
    respiratoryProblem = respiratoryProblem.split(',');
    let z;
    for (z = 0; z < respiratoryProblem.length; z++){
      respiratoryProblemRow[z].innerHTML = `<td> ${respiratoryProblem[z]} </td><td>YES</td>`;
    }

}