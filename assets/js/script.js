const result = [];
const closeContactResult = [];
const respiratoryProblemResult = [];



function nonTravellerPatient(){
	let patSympCounter = 0 ;
	let closeContactCounter = 0;
	let respiratoryProblemCounter = 0;
	let notTravAndCloseContact = 0;
	let occurrenceCounter = 0;
	patientSymptoms();

		
	document.querySelector("#proceed").addEventListener('click', () =>{
		if (patSympCounter < 1) { 
			window.location.href= "./summary.html"
			localStorage.removeItem("patientSymptoms", result);
			localStorage.setItem("final", "pum");
		}
		else {
			localStorage.setItem("patientSymptoms", result);
			notTraveller()
		}
	})
	
	function patientSymptoms(){	
		
		const patientSymptoms = [
			"Do you have a fever more than or equal to 38C? ",
			"Do you have a cough? ",
			"Are you experiencing shortness of breath? ",
			"Do you have a any other respiratory symptoms? "
		];

		patSymp = document.getElementsByName('patSymp')

		patSymp[0].addEventListener('click', function() {
		console.log(this.checked, "patientSymptoms0", " -- ",patSympCounter);
		if(this.checked === true) {
			//document.getElementsByClassName("choices")[0].getElementById("patSymp1").style.border = "2px solid #A93226 !important";
			result.push(patientSymptoms[0])
			patSympCounter += 1
		}
		else {
			strToBeDeleted = patientSymptoms[0];
			for (i=0; i<result.length; i++){
				if (strToBeDeleted == result[i]) {
					console.log(i);
				break;	
				}
			}
			// result.pop(patientSymptoms[0])
			result.splice(i,1);
			patSympCounter -= 1
			
		}

		console.log(result)
		})


		patSymp[1].addEventListener('click', function() {
		console.log(this.checked, "patientSymptoms1", " -- ",patSympCounter);
		if(this.checked === true) {
			result.push(patientSymptoms[1])
			patSympCounter += 1
		}
		else {
			strToBeDeleted = patientSymptoms[1];
			for (i=0; i<result.length; i++){
				if (strToBeDeleted == result[i]) {
					console.log(i);
				break;	
				}
			}
			// result.pop(patientSymptoms[0])
			result.splice(i,1);
			patSympCounter -= 1
		}

		console.log(result)
		})


		patSymp[2].addEventListener('click', function() {
		console.log(this.checked, "patientSymptoms2", " -- ",patSympCounter);
		if(this.checked === true) {
			result.push(patientSymptoms[2])
			patSympCounter += 1
		}
		else {
			strToBeDeleted = patientSymptoms[2];
			for (i=0; i<result.length; i++){
				if (strToBeDeleted == result[i]) {
					console.log(i);
				break;	
				}
			}
			// result.pop(patientSymptoms[0])
			result.splice(i,1);
			patSympCounter -= 1
		}

		console.log(result)
		})


		patSymp[3].addEventListener('click', function() {
		console.log(this.checked, "patientSymptoms3", " -- ",patSympCounter);
		if(this.checked === true) {
			result.push(patientSymptoms[3])
			patSympCounter += 1
		}
		else {
			strToBeDeleted = patientSymptoms[3];
			for (i=0; i<result.length; i++){
				if (strToBeDeleted == result[i]) {
					console.log(i);
				break;	
				}
			}
			// result.pop(patientSymptoms[0])
			result.splice(i,1);
			patSympCounter -= 1
		}

		console.log(result)	
		})


		patSymp[4].addEventListener('click', function() {
		console.log(this.checked, "patientSymptoms3", " -- ",patSympCounter);
		if(this.checked === true) {
			localStorage.removeItem("patientSymptoms", result);
			patSympCounter = 0;
		}
		
		})

		return patSympCounter;
	}

	function notTraveller(){
		document.querySelector("#notTraveller").style.display = "block";
		document.querySelector("#patientSymptoms").style.display = "none";
		document.querySelector("#proceed").disabled = true;
		document.querySelector("#nTravYes").addEventListener('click', ()=>{
			localStorage.setItem("nTravYes", document.querySelector("#nTravYes").innerHTML);
			localStorage.setItem("nTravYesQ", "Have you traveled or are you residing in a country/area reporting local transmission of COVID-19?");
			// localStorage.removeItem("nTravNo", document.querySelector("#nTravNo").innerHTML);
			// localStorage.removeItem("nTravNoQ", "Have you traveled or are you residing in a country/area reporting local transmission of COVID-19?");
			document.querySelector("#proceed").disabled = false;
			notTravAndCloseContact++
		})
		document.querySelector("#nTravNo").addEventListener('click', ()=>{
			// localStorage.setItem("nTravNo", document.querySelector("#nTravNo").innerHTML);
			// localStorage.setItem("nTravNoQ", "Have you traveled or are you residing in a country/area reporting local transmission of COVID-19?");
			localStorage.removeItem("nTravYes", document.querySelector("#nTravYes").innerHTML);
			localStorage.removeItem("nTravYesQ", "Have you traveled or are you residing in a country/area reporting local transmission of COVID-19?");
			document.querySelector("#proceed").disabled = false;	
		})
		document.querySelector("#proceed").addEventListener('click', () =>{
		closeContact();
		})
	}

	function closeContact(){
		document.querySelector("#proceed").disabled = false;
		document.querySelector("#closeContact").style.display = "block";
		document.querySelector("#notTraveller").style.display = "none";
		
		document.querySelector("#proceed").addEventListener('click', () =>{
			sumNotTravNClsCnt = notTravAndCloseContact + closeContactCounter;
			console.log(sumNotTravNClsCnt);
			if (sumNotTravNClsCnt > 0) {
				localStorage.setItem("closeContact", closeContactResult);
				occurrence();
			}
			else {
				respiratoryProblem();
			}
			
		})
		const closeContact = [
			"Have had any CLOSE CONTACT with a confirmed COVID-19 case? ",
			"Have you provided direct care without proper PPE to confirmed COVID-19 patient? ",
			"Have stayed in the same close environment with a confirmed COVID-19 patient? ",
			"Have you traveled together with a confirmed COVID-19 patient in close priximity (1 meter ot 3 feet) in any kind of conveyance "
		]

		closeContacts = document.getElementsByName('closeContact')

		closeContacts[0].addEventListener('click', function() {
		console.log(this.checked, "closeContact0")
		if(this.checked === true) {
			closeContactResult.push(closeContact[0])
			closeContactCounter += 1

		}
		else {
			strToBeDeleted = closeContact[0];
			for (i=0; i<closeContactResult.length; i++){
				if (strToBeDeleted == closeContactResult[i]) {
					console.log(i);
				break;	
				}
			}
			// closeContactResult.pop(patientSymptoms[0])
			closeContactResult.splice(i,1);
			closeContactCounter -= 1
		}
		console.log(closeContactResult)
		})

		closeContacts[1].addEventListener('click', function() {
		console.log(this.checked, "closeContact1")
		if(this.checked === true) {
			closeContactResult.push(closeContact[1])
			closeContactCounter += 1

		}
		else {
			strToBeDeleted = closeContact[1];
			for (i=0; i<closeContactResult.length; i++){
				if (strToBeDeleted == closeContactResult[i]) {
					console.log(i);
				break;	
				}
			}
			// closeContactResult.pop(patientSymptoms[0])
			closeContactResult.splice(i,1);
			closeContactCounter -= 1
		}
		console.log(closeContactResult)
		})

		closeContacts[2].addEventListener('click', function() {
		console.log(this.checked, "closeContact2")
		if(this.checked === true) {
			closeContactResult.push(closeContact[2])
			closeContactCounter += 1

		}
		else {
			strToBeDeleted = closeContact[2];
			for (i=0; i<closeContactResult.length; i++){
				if (strToBeDeleted == closeContactResult[i]) {
					console.log(i);
				break;	
				}
			}
			// closeContactResult.pop(patientSymptoms[0])
			closeContactResult.splice(i,1);
			closeContactCounter -= 1
		}
		console.log(closeContactResult)
		})

		closeContacts[3].addEventListener('click', function() {
		console.log(this.checked, "closeContact3")
		if(this.checked === true) {
			closeContactResult.push(closeContact[3])
			closeContactCounter += 1

		}
		else {
			strToBeDeleted = closeContact[3];
			for (i=0; i<closeContactResult.length; i++){
				if (strToBeDeleted == closeContactResult[i]) {
					console.log(i);
				break;	
				}
			}
			// closeContactResult.pop(patientSymptoms[0])
			closeContactResult.splice(i,1);
			closeContactCounter -= 1
		}
		console.log(closeContactResult)
		})
	
		closeContacts[4].addEventListener('click', function() {
		console.log(this.checked, "closeContact4")
		if(this.checked === true) {
			//document.querySelector("#proceed").disabled = false;
			localStorage.removeItem("closeContact", closeContactResult);
			closeContactCounter = 0

		}
		// console.log(closeContactResult)
		})
	}

	function occurrence(){
		document.querySelector("#closeContact").style.display = "none";
		document.querySelector("#occurrence").style.display = "block";
		document.querySelector("#proceed").disabled = true;
		document.querySelector("#proceed").addEventListener("click", ()=>{
		
			window.location.href= "./summary.html"
			
			
		})
		document.querySelector("#occyes").addEventListener('click', ()=>{
			localStorage.setItem("occyes", document.querySelector("#occyes").innerHTML);
			localStorage.setItem("occyesQ", "Did the symptoms occur within 14 days of exposure?");
			localStorage.setItem("final", "pui");
			document.querySelector("#proceed").disabled = false;
			// localStorage.removeItem("occno", document.querySelector("#occno").innerHTML);
			// localStorage.removeItem("occnoQ", "Did the symptoms occur within 14 days of exposure?");
			

		})
		document.querySelector("#occno").addEventListener('click', ()=>{
			// localStorage.setItem("occno", document.querySelector("#occno").innerHTML);
			// localStorage.setItem("occnoQ", "Did the symptoms occur within 14 days of exposure?");
			localStorage.setItem("final", "npuipum");
			localStorage.removeItem("occyes", document.querySelector("#occyes").innerHTML);
			localStorage.removeItem("occyesQ", "Did the symptoms occur within 14 days of exposure?");
			document.querySelector("#proceed").disabled = false;
		})
	}

	function respiratoryProblem(){
		document.querySelector("#proceed").disabled = true;
		document.querySelector("#respiratoryProblem").style.display = "block"
		document.querySelector("#closeContact").style.display = "none"

		document.querySelector("#proceed").addEventListener("click", ()=>{
			if (respiratoryProblemCounter < 1) {
				localStorage.removeItem("respiratoryProblem",respiratoryProblemResult);
				localStorage.setItem("final", "npuipum");
				window.location.href= "./summary.html"
			}
			else {
				localStorage.setItem("respiratoryProblem", respiratoryProblemResult);
				localStorage.setItem("final", "pui");
				window.location.href= "./summary.html"
			}
		})

		const respiratoryProblem = [
			"Has severe acute respiratory infection or atypical pneumonia AND requiring hospitalization AND with no other etiology to fully explain the clinical presentation regardless of exposure history.",
			"Has cluster(atleast 5) of ILI(Influenza-Like Illness) cases in household or workplace."
			]

		respiratoryProblems = document.getElementsByName('resProb')

		respiratoryProblems[0].addEventListener('click', function() {
		console.log(this.checked, "respiratoryProblem")
		if(this.checked === true) {
			respiratoryProblemResult.push(respiratoryProblem[0])
			respiratoryProblemCounter += 1
			document.querySelector("#proceed").disabled = false;
		}
		else {
			strToBeDeleted = respiratoryProblem[0];
			for (i=0; i<respiratoryProblemResult.length; i++){
				if (strToBeDeleted == respiratoryProblemResult[i]) {
					console.log(i);

				break;	
				}
			}
			// respiratoryProblemResult.pop(patientSymptoms[0])
			respiratoryProblemResult.splice(i,1);
			respiratoryProblemCounter -= 1
			document.querySelector("#proceed").disabled = true;
		}
		console.log(respiratoryProblemResult)
		})

		respiratoryProblems[1].addEventListener('click', function() {
		console.log(this.checked, "respiratoryProblem1")
		if(this.checked === true) {
			respiratoryProblemResult.push(respiratoryProblem[1])
			respiratoryProblemCounter += 1
			document.querySelector("#proceed").disabled = false;
		}
		else {
			strToBeDeleted = respiratoryProblem[1];
			for (i=0; i<respiratoryProblemResult.length; i++){
				if (strToBeDeleted == respiratoryProblemResult[i]) {
					console.log(i);
				break;	
				}
			}
			// respiratoryProblemResult.pop(patientSymptoms[0])
			respiratoryProblemResult.splice(i,1);
			respiratoryProblemCounter -= 1
			document.querySelector("#proceed").disabled = true;
		}
		console.log(respiratoryProblemResult)
		})
	
		respiratoryProblems[2].addEventListener('click', function() {
		console.log(this.checked, "respiratoryProblem1")
		if(this.checked === true) {
			document.querySelector("#proceed").disabled = false;
			localStorage.removeItem("respiratoryProblem",respiratoryProblemResult);
			respiratoryProblemCounter = 0

		}
		// else {
		// 	strToBeDeleted = respiratoryProblem[1];
		// 	for (i=0; i<respiratoryProblemResult.length; i++){
		// 		if (strToBeDeleted == respiratoryProblemResult[i]) {
		// 			console.log(i);
		// 		break;	
		// 		}
		// 	}
		// 	// respiratoryProblemResult.pop(patientSymptoms[0])
		// 	respiratoryProblemResult.splice(i,1);
		// 	respiratoryProblemCounter -= 1
		// }
		// console.log(respiratoryProblemResult)
		})
	}

}




function travel(){
	const forms = [
		document.querySelector("#travntrav"),
		document.querySelector("#traveller"),
		document.querySelector("#exposure"),
		document.querySelector("#symptoms"),
	];

	document.querySelector("#traveller").style.display = "block";
	document.querySelector("#symptoms").style.display = "block";
	document.querySelector("#exposure").style.display = "block";
	document.querySelector("#proceed").style.visibility = "visible";

	let counter = 1;
	document.querySelector("#proceed").addEventListener("click", ()=>{

		if (counter < forms.length-1) {
		counter++
		forms[counter-1].style.display = "none";
		   forms[counter].style.display = "block";

		}
	})

}

function ntravel(){
	// const forms = [
	// document.querySelector("#travntrav"),
	// document.querySelector("#notTraveller"),
	// document.querySelector("#patientSymptoms"),
	// document.querySelector("#closeContact"),
	// document.querySelector("#occurrence"),
	// document.querySelector("#respiratoryProblem"),
	// ];
	// document.querySelector("#notTraveller").style.display = "none";
	document.querySelector("#patientSymptoms").style.display = "block";
	// document.querySelector("#closeContact").style.display = "none";
	// document.querySelector("#occurrence").style.display = "none";
	// document.querySelector("#respiratoryProblem").style.display = "none";
	document.querySelector("#proceed").style.visibility = "visible";
	nonTravellerPatient();

	// let counter	= 1;
	// document.querySelector("#proceed").addEventListener("click", ()=>{
	// 	if  (counter < forms.length-1) {
	//    		counter++
	//    		forms[counter-1].style.display = "none";
	//         forms[counter].style.display = "block";

	//    	}

	// })
	
}


function travntrav(){

	document.querySelector("#ntravel").addEventListener("click", ()=>{
		console.log(document.querySelector("#name").value);
		const	name = document.querySelector("#name");
				
			if (name.value == "") {
				alert('enter name')
			}
			else{
				document.querySelector("#travntrav").style.display = "none";
				ntravel();
				localStorage.setItem("name",name.value)
			}
	})
	document.querySelector("#travel").addEventListener("click", ()=>{
		console.log(document.querySelector("#name").value);
		const	name = document.querySelector("#name");
				
			if (name.value == "") {
				alert('enter name')
			}
			else{
				document.querySelector("#travntrav").style.display = "none";
				localStorage.setItem("name",name.value)
				travel();
			}
	})


}

travntrav();
//  =================== end travellers script =======================

//  =================== start patient script =======================

// const patientSymptoms = [
// 	"Do you have a fever more than or equal to 38C? ",
// 	"Do you have a cough? ",
// 	"Are you experiencing shortness of breath? ",
// 	"Do you have a any other respiratory symptoms? "
// ]
// const closeContact = [
// 	"Have you traveled or are you residing in a country/area reporting local transmission of COVID-19? ",
// 	"Have had any CLOSE CONTACT with a confirmed COVID-19 case? ",
// 	"Have you provided direct care without proper PPE to confirmed COVID-19 patient? ",
// 	"Have stayed in the same close environment (incl. workplace, classroom, houshold, gatherings) with a confirmed COVID-19 patient? ",
// 	"Have you traveled together with a confirmed COVID-19 patient in close priximity (1 meter ot 3 feet) in any kind of conveyance "
// ]

// // check patient symptoms
// function checkPatientSymptoms() {
// 	// counter = 1
// 	// question = patientSymptoms
// 	// while (counter < question.length + 1){
// 	// 	let correctInput = false
// 	// 		while (correctInput == false) {
// 	// 			// this generate qestions  
// 	// 			 userInput = prompt(question[counter-1])

// 	// 			 userInput =  userInput.trim().toLowerCase()

// 	// 			 // validate userInput
// 	// 			userInput === "yes"|| userInput === "no" ?
// 	// 			correctInput = true
// 	// 			:errinput()
// 	// 		}
// 	// 		document.querySelector("#patientSymptoms"+counter).innerHTML = "<td>" + question[counter-1] + "</td>"+ "<td>" + userInput + "</td>";

// 	// 		userInput === "yes" ? 
// 	// 		counter = question.length + 1
// 	// 		:counter +=1
// 	// 	}

// 	// userInput === "yes" ?
// 	// checkCloseContact()
// 	// :document.querySelector("#sum").innerHTML = status.pum
// 	if (document.querySelector("#sympyes").addEventListener("click"))=true {
// 		document.querySelector("#sympyes").addEventListener("click", ()=>{
			
// 			localStorage.setItem("sympyes", document.querySelector("#sympyes").innerHTML);
// 			localStorage.setItem("sympyesQ", "Is your body temparature 38 and above? OR Do you have any respiratory illness such as cold or cough?");
				
// 	})
// 	document.querySelector("#sympno").addEventListener("click", ()=>{
// 			localStorage.setItem("sympno", document.querySelector("#sympno").innerHTML);
// 			localStorage.setItem("sympnoQ", "Is your body temparature 38 and above? OR Do you have any respiratory illness such as cold or cough?");
		
// 	})
// 		// if (sympno.checked = true){
// 		// 	sympno.innerHTML = "checked"
// 		// 	localStorage.setItem("sympno", document.querySelector("#sympno").innerHTML);
// 		// 	localStorage.setItem("sympnoQ", "Is your body temparature 38 and above? OR Do you have any respiratory illness such as cold or cough?");
// 		// }
// 		// else if (sympyes.checked = true) {
// 		// 	sympyes.innerHTML = "checked"
// 		// 	//document.querySelector("#sum").innerHTML = `<td> Is your body temparature 38 and above? OR Do you have any respiratory illness such as cold or cough? </td><td>YES</td>`;
// 		// 	localStorage.setItem("sympyes", document.querySelector("#sympyes").innerHTML);
// 		// 	localStorage.setItem("sympyesQ", "Is your body temparature 38 and above? OR Do you have any respiratory illness such as cold or cough?");
// 		// 	// window.location.href="./summary.html";
// 		// }


// }

// // check direct contact
// function checkCloseContact() {
// 	counter = 1
// 	question = closeContact
// 	while (counter < question.length + 1){
// 		let correctInput = false
// 			while (correctInput == false) {
// 				// this generate qestions  
// 				userInput = prompt(question[counter-1])

// 				userInput =  userInput.trim().toLowerCase()

// 				 // validate userInput
// 				userInput === "yes"|| userInput === "no" ?
// 				correctInput = true
// 				:errinput()
// 			}
// 			document.querySelector("#closeContact"+counter).innerHTML = "<td>" + question[counter-1] + "</td>"+ "<td>" + userInput + "</td>";

// 			userInput === "yes" ? 
// 			counter = question.length + 1
// 			:counter +=1
// 		}

// 	userInput === "yes" ?
// 	checkDaysOfExposure() // ifsick()
// 	:checkSevereAcuteRespiratory()
// }

// // Did symptoms occur within 14 days of exposure
// function checkDaysOfExposure(){
// 	correctInput = false
// 	question = "Did the symptoms occur within 14 days of exposure? "
// 	while (correctInput == false) {
// 		userInput = prompt(question)
		
// 		userInput =  userInput.trim().toLowerCase()

// 		 // validate userInput
// 		userInput === "yes"|| userInput === "no" ?
// 		correctInput = true
// 		:errinput()
// 	}
// 	document.querySelector("#patientExposure").innerHTML = "<td>" + question + "</td>"+ "<td>" + userInput + "</td>";

// 	userInput === 'yes' ?
// 	document.querySelector("#sum").innerHTML = status.pui
// 	: document.querySelector("#sum").innerHTML = status.notPuiPum
// }

// // check patient for Severe Acute Respiratory
// function checkSevereAcuteRespiratory(){
// 	correctInput = false
// 	question = "Do you have severe acute respiratory infection or atypical pneumonia? "
// 	while (correctInput == false) {
// 		userInput = prompt(question)
		
// 		userInput =  userInput.trim().toLowerCase()

// 		 // validate userInput
// 		userInput === "yes"|| userInput === "no" ?
// 		correctInput = true
// 		:errinput()
// 	}
// 	document.querySelector("#severAcuteRespiratory").innerHTML = "<td>" + question + "</td>"+ "<td>" + userInput + "</td>";

// 	userInput === 'yes' ?
// 	document.querySelector("#sum").innerHTML = status.pui
// 	: document.querySelector("#sum").innerHTML = status.notPuiPum
// }

//  =================== end patient script =======================

// choose patient or traveller
// function start() {
// 	correctInput = false
// 	while (correctInput == false) {
// 		userInput = prompt("Enter \n 'P' for Patient \n 'T' for Traveller:")

// 		userInput =  userInput.trim().toLowerCase()

// 		userInput === 'p' || userInput === 't' ?
// 		correctInput = true
// 		: alert("Enter 'P' or 'T' only")
// 	}

// 	userInput == 'p' ?
// 	checkPatientSymptoms()
// 	: checkTravellerExposure()
// }

// start()
//  =================== end of proposal script as per Algorithm of DOH dated March 16, 2020 =======================



// let nonTrav = function(){

// 	let nonA, nonB, nonC, nonD, nonE, nonF, nonG, nonH, nonI, nonJ, nonK, nonL;
// 	// symptoms
// 	//nonA
// 	let checker = false;
// 	while (checker == false){
// 		nonA = prompt("Do you have a fever more than or equal to 38C? (yes or no)","yes or no");
// 		nonA = nonA.toLowerCase();
// 		nonA = nonA.trim();
		
// 		if (nonA == 'yes' || nonA == 'no'){
			
		
// 	    checker = true;
	  
// 		}
// 		else {
			
// 			alert('input "yes" or "no" only');
// 		}
// 		}

// 	// nonB
// 	checker = false
// 	while (checker == false){
// 		nonB = prompt("Do you have a cough? (yes or no)","yes or no");
// 		nonB = nonB.toLowerCase();
// 		nonB = nonB.trim();
		
// 		if (nonB == 'yes' || nonB == 'no'){
// 			checker = true;

// 		}
// 		else {
			
// 			alert('input "yes" or "no" only')
// 		}
// 		}

// 	// nonC
// 	checker = false
// 	while (checker == false){
// 		nonC = prompt("Are you experiencing shortness of breath? (yes or no)","yes or no");
// 		nonC = nonC.toLowerCase();
// 		nonC = nonC.trim();
		
// 		if (nonC == 'yes' || nonC == 'no'){
// 			checker = true;

// 		}
// 		else {
			
// 			alert('input "yes" or "no" only')
// 		}
// 		}

// 	// nonD
// 	checker = false
// 	while (checker == false){
// 		nonD = prompt("Do you have a any other respiratory symptoms? (yes or no)","yes or no");
// 		nonD = nonD.toLowerCase();
// 		nonD = nonD.trim();
		
// 		if (nonD == 'yes' || nonD == 'no'){
// 			checker = true;

// 		}
// 		else {
			
// 			alert('input "yes" or "no" only')
// 		}
// 		}

// 	// nonE
// 	checker = false
// 	while (checker == false){
// 		nonE = prompt("Do you have a diarrhea? (yes or no)","yes or no");
// 		nonE = nonE.toLowerCase();
// 		nonE = nonE.trim();
		
// 		if (nonE == 'yes' || nonE == 'no'){
// 			checker = true;

// 		}
// 		else {
			
// 			alert('input "yes" or "no" only')
// 		}
// 		}


// 	if (nonA == 'yes' || nonB == 'yes' || nonC == 'yes' || nonD == 'yes' || nonE == 'yes'){
		
// 		ifsick();
		
// 	 }
// 	else {
// 		console.log('You are neither PUI nor PUM');	
		 
// 		document.getElementById("nonA").innerHTML =
// 		 "<td>" + "Do you have a fever more than or equal to 38C?" + "</td>"+
//  		 "<td>" + nonA + "</td>";
// 		document.getElementById("nonB").innerHTML =
// 		 "<td>" + "Do you have a cough?" + "</td>"+
//  		 "<td>" + nonB + "</td>";
// 		document.getElementById("nonC").innerHTML =
// 		 "<td>" + "Are you experiencing shortness of breath?" + "</td>"+
//  		 "<td>" + nonC + "</td>";
// 		document.getElementById("nonD").innerHTML =
// 		 "<td>" + "Do you have a any other respiratory symptoms?" + "</td>"+
//  		 "<td>" + nonD + "</td>";
// 		document.getElementById("nonE").innerHTML =
// 		 "<td>" + "Do you have a diarrhea?" + "</td>"+
//  		 "<td>" + nonE + "</td>";
			

// 		 // document.getElementById("summary").innerHTML =
// 	  //   "Do you have a fever more than or equal to 38C? " + nonA + "<br>" +
// 	  //   "Do you have a cough? " + nonB + "<br>" +
// 	  //   "Are you experiencing shortness of breath? " + nonC + "<br>" +
// 	  //   "Do you have a any other respiratory symptoms? " + nonD + "<br>" +
// 	  //   "Do you have a diarrhea? " + nonE;
// 	    document.getElementById("sum").innerHTML =
//     	"You are neither PUI or PUM. Stay healthy. Observe Social Distancing"
// 		}



// 	// if symptom/s is/are yes
// 	function ifsick(){
		
// 		// nonF
// 		checker = false
// 		while (checker == false){
// 			nonF = prompt("Have you traveled or are you residing in a country/area reporting local transmission of COVID-19? (yes or no)","yes or no");
// 			nonF = nonF.toLowerCase();
// 			nonF = nonF.trim();
			
// 			if (nonF == 'yes' || nonF == 'no'){
// 				checker = true;

// 			}
// 			else {
				
// 				alert('input "yes" or "no" only')
// 			}
// 			}

		
// 		// nonG
// 		checker = false
// 		while (checker == false){
// 			nonG = prompt("Have had any CLOSE CONTACT with a confirmed COVID-19 case? (yes or no)", "yes or no");
// 			nonG = nonG.toLowerCase();
// 			nonG = nonG.trim();
			
// 			if (nonG == 'yes' || nonG == 'no'){
// 				checker = true;

// 			}
// 			else {
				
// 				alert('input "yes" or "no" only')
// 			}
// 			}

		
// 		// nonH
// 		checker = false
// 		while (checker == false){
// 			nonH = prompt("Have you provided direct care without proper PPE to confirmed COVID-19 patient? (yes or no)", "yes or no");
// 			nonH = nonH.toLowerCase();
// 			nonH = nonH.trim();
			
// 			if (nonH == 'yes' || nonH == 'no'){
// 				checker = true;

// 			}
// 			else {
				
// 				alert('input "yes" or "no" only')
// 			}
// 			}

			
// 		// nonI
// 		checker = false
// 		while (checker == false){
// 			nonI = prompt("Have stayed in the same close environment (incl. workplace, classroom, houshold, gatherings) with a confirmed COVID-19 patient? (yes or no)", "yes or no");
// 			nonI = nonI.toLowerCase();
// 			nonI = nonI.trim();
			
// 			if (nonI == 'yes' || nonI == 'no'){
// 				checker = true;

// 			}
// 			else {
				
// 				alert('input "yes" or "no" only')
// 			}
// 			}

		
// 		// nonJ
// 		checker = false
// 		while (checker == false){
// 			nonJ = prompt("Have you traveled together with a confirmed COVID-19 patient in close priximity (1 meter ot 3 feet) in any kind of conveyance (yes or no)", "yes or no");
// 			nonJ = nonJ.toLowerCase();
// 			nonJ = nonJ.trim();
			
// 			if (nonJ == 'yes' || nonJ == 'no'){
// 				checker = true;

// 			}
// 			else {
				
// 				alert('input "yes" or "no" only')
// 			}
// 			}

		
		
// 		if (nonF == 'yes' || nonG == 'yes' || nonH == 'yes' || nonI == 'yes' || nonJ == 'yes'){

// 			//if ifsick yes		
// 			// nonK
// 			checker = false
// 			while (checker == false){
// 				nonK = prompt("Did the symptoms occur within 14 days of exposure? (yes or no)", "yes or no");
// 				nonK = nonK.toLowerCase();
// 				nonK = nonK.trim();
				
// 				if (nonK == 'yes' || nonK == 'no'){
// 					checker = true;

// 				}
// 				else {
					
// 					alert('input "yes" or "no" only')
// 				}
// 				}

// 				// if nonK yes
// 			if (nonK == 'yes') {
				
// 				alert('You are categorized as PUI(Person Under Investigation)');
// 				document.getElementById("nonA").innerHTML =
// 				 "<td>" + "Do you have a fever more than or equal to 38C?" + "</td>"+
// 		 		 "<td>" + nonA + "</td>";
// 				document.getElementById("nonB").innerHTML =
// 				 "<td>" + "Do you have a cough?" + "</td>"+
// 		 		 "<td>" + nonB + "</td>";
// 				document.getElementById("nonC").innerHTML =
// 				 "<td>" + "Are you experiencing shortness of breath?" + "</td>"+
// 		 		 "<td>" + nonC + "</td>";
// 				document.getElementById("nonD").innerHTML =
// 				 "<td>" + "Do you have a any other respiratory symptoms?" + "</td>"+
// 		 		 "<td>" + nonD + "</td>";
// 				document.getElementById("nonE").innerHTML =
// 				 "<td>" + "Do you have a diarrhea?" + "</td>"+
// 		 		 "<td>" + nonE + "</td>";
// 				document.getElementById("nonF").innerHTML =
// 				 "<td>" + "Have you traveled or are you residing in a country/area reporting local transmission of COVID-19?" + "</td>"+
// 		 		 "<td>" + nonF + "</td>";
// 				document.getElementById("nonG").innerHTML =
// 				 "<td>" + "Have had any CLOSE CONTACT with a confirmed COVID-19 case?" + "</td>"+
// 		 		 "<td>" + nonG + "</td>";
// 				document.getElementById("nonH").innerHTML =
// 				 "<td>" + "Have you provided direct care without proper PPE to confirmed COVID-19 patient?" + "</td>"+
// 		 		 "<td>" + nonH + "</td>";
// 				document.getElementById("nonI").innerHTML =
// 				 "<td>" + "Have stayed in the same close environment (incl. workplace, classroom, houshold, gatherings) with a confirmed COVID-19 patient?" + "</td>"+
// 		 		 "<td>" + nonI + "</td>";
// 				document.getElementById("nonJ").innerHTML =
// 				 "<td>" + "Have you traveled together with a confirmed COVID-19 patient in close priximity (1 meter ot 3 feet) in any kind of conveyance" + "</td>"+
// 		 		 "<td>" + nonJ + "</td>";
// 				document.getElementById("nonK").innerHTML =
// 				 "<td>" + "Did the symptoms occur within 14 days of exposure?" + "</td>"+
// 		 		 "<td>" + nonK + "</td>";
// 				// document.getElementById("summary").innerHTML =
// 				// "Do you have a fever more than or equal to 38C? " + nonA + "<br>" +
// 				// "Do you have a cough? " + nonB + "<br>" +
// 				// "Are you experiencing shortness of breath? " + nonC + "<br>" +
// 				// "Do you have a any other respiratory symptoms? " + nonD + "<br>" +
// 				// "Do you have a diarrhea? " + nonE + "<br>" +
// 				// "Have you traveled or are you residing in a country/area reporting local transmission of COVID-19? " + nonF + "<br>" +
// 				// "Have had any CLOSE CONTACT with a confirmed COVID-19 case? " + nonG + "<br>" +
// 				// "Have you provided direct care without proper PPE to confirmed COVID-19 patient? " + nonH + "<br>" +
// 				// "Have stayed in the same close environment (incl. workplace, classroom, houshold, gatherings) with a confirmed COVID-19 patient? " + nonI + "<br>" +
// 				// "Have you traveled together with a confirmed COVID-19 patient in close priximity (1 meter ot 3 feet) in any kind of conveyance " + nonJ + "<br>" +
// 				// "Did the symptoms occur within 14 days of exposure? " + nonK;
// 				document.getElementById("sum").innerHTML =
//     			"You are categorized as PUI. Please seek medical assistance immediately."
// 			 }
			
// 			 // if nonK no
// 			else{
			
// 				alert("You are neither PUI nor PUM");
// 				document.getElementById("nonA").innerHTML =
// 				 "<td>" + "Do you have a fever more than or equal to 38C?" + "</td>"+
// 		 		 "<td>" + nonA + "</td>";
// 				document.getElementById("nonB").innerHTML =
// 				 "<td>" + "Do you have a cough?" + "</td>"+
// 		 		 "<td>" + nonB + "</td>";
// 				document.getElementById("nonC").innerHTML =
// 				 "<td>" + "Are you experiencing shortness of breath?" + "</td>"+
// 		 		 "<td>" + nonC + "</td>";
// 				document.getElementById("nonD").innerHTML =
// 				 "<td>" + "Do you have a any other respiratory symptoms?" + "</td>"+
// 		 		 "<td>" + nonD + "</td>";
// 				document.getElementById("nonE").innerHTML =
// 				 "<td>" + "Do you have a diarrhea?" + "</td>"+
// 		 		 "<td>" + nonE + "</td>";
// 				document.getElementById("nonF").innerHTML =
// 				 "<td>" + "Have you traveled or are you residing in a country/area reporting local transmission of COVID-19?" + "</td>"+
// 		 		 "<td>" + nonF + "</td>";
// 				document.getElementById("nonG").innerHTML =
// 				 "<td>" + "Have had any CLOSE CONTACT with a confirmed COVID-19 case?" + "</td>"+
// 		 		 "<td>" + nonG + "</td>";
// 				document.getElementById("nonH").innerHTML =
// 				 "<td>" + "Have you provided direct care without proper PPE to confirmed COVID-19 patient?" + "</td>"+
// 		 		 "<td>" + nonH + "</td>";
// 				document.getElementById("nonI").innerHTML =
// 				 "<td>" + "Have stayed in the same close environment (incl. workplace, classroom, houshold, gatherings) with a confirmed COVID-19 patient?" + "</td>"+
// 		 		 "<td>" + nonI + "</td>";
// 				document.getElementById("nonJ").innerHTML =
// 				 "<td>" + "Have you traveled together with a confirmed COVID-19 patient in close priximity (1 meter ot 3 feet) in any kind of conveyance" + "</td>"+
// 		 		 "<td>" + nonJ + "</td>";
// 				document.getElementById("nonK").innerHTML =
// 				 "<td>" + "Did the symptoms occur within 14 days of exposure?" + "</td>"+
// 		 		 "<td>" + nonK + "</td>";
// 				// document.getElementById("summary").innerHTML =
// 				// "Do you have a fever more than or equal to 38C? " + nonA + "<br>" +
// 				// "Do you have a cough? " + nonB + "<br>" +
// 				// "Are you experiencing shortness of breath? " + nonC + "<br>" +
// 				// "Do you have a any other respiratory symptoms? " + nonD + "<br>" +
// 				// "Do you have a diarrhea? " + nonE + "<br>" +
// 				// "Have you traveled or are you residing in a country/area reporting local transmission of COVID-19? " + nonF + "<br>" +
// 				// "Have had any CLOSE CONTACT with a confirmed COVID-19 case? " + nonG + "<br>" +
// 				// "Have you provided direct care without proper PPE to confirmed COVID-19 patient? " + nonH + "<br>" +
// 				// "Have stayed in the same close environment (incl. workplace, classroom, houshold, gatherings) with a confirmed COVID-19 patient? " + nonI + "<br>" +
// 				// "Have you traveled together with a confirmed COVID-19 patient in close priximity (1 meter ot 3 feet) in any kind of conveyance " + nonJ + "<br>" +
// 				// "Did the symptoms occur within 14 days of exposure? " + nonK;
// 				document.getElementById("sum").innerHTML =
//     			"You are neither PUI or PUM. Stay healthy. Observe Social Distancing"
// 			 }
// 		}

// 		else {
// 			// // if ifsick no
// 			checker = false
// 			while (checker == false){
// 				nonL = prompt("Do you have severe acute respiratory infection or atypical pneumonia? (yes or no)", "yes or no");
// 				nonL = nonL.toLowerCase();
// 				nonL = nonL.trim();
				
// 				if (nonL == 'yes' || nonL == 'no'){
// 					checker = true;

// 				}
// 				else {
					
// 					alert('input "yes" or "no" only')
// 				}
// 				}	
			
			
// 			if (nonL == 'yes') {
// 				// // if nonL yes
// 				alert('You are categorized as PUI(Person Under Investigation)');
// 				document.getElementById("nonA").innerHTML =
// 				 "<td>" + "Do you have a fever more than or equal to 38C?" + "</td>"+
// 		 		 "<td>" + nonA + "</td>";
// 				document.getElementById("nonB").innerHTML =
// 				 "<td>" + "Do you have a cough?" + "</td>"+
// 		 		 "<td>" + nonB + "</td>";
// 				document.getElementById("nonC").innerHTML =
// 				 "<td>" + "Are you experiencing shortness of breath?" + "</td>"+
// 		 		 "<td>" + nonC + "</td>";
// 				document.getElementById("nonD").innerHTML =
// 				 "<td>" + "Do you have a any other respiratory symptoms?" + "</td>"+
// 		 		 "<td>" + nonD + "</td>";
// 				document.getElementById("nonE").innerHTML =
// 				 "<td>" + "Do you have a diarrhea?" + "</td>"+
// 		 		 "<td>" + nonE + "</td>";
// 				document.getElementById("nonF").innerHTML =
// 				 "<td>" + "Have you traveled or are you residing in a country/area reporting local transmission of COVID-19?" + "</td>"+
// 		 		 "<td>" + nonF + "</td>";
// 				document.getElementById("nonG").innerHTML =
// 				 "<td>" + "Have had any CLOSE CONTACT with a confirmed COVID-19 case?" + "</td>"+
// 		 		 "<td>" + nonG + "</td>";
// 				document.getElementById("nonH").innerHTML =
// 				 "<td>" + "Have you provided direct care without proper PPE to confirmed COVID-19 patient?" + "</td>"+
// 		 		 "<td>" + nonH + "</td>";
// 				document.getElementById("nonI").innerHTML =
// 				 "<td>" + "Have stayed in the same close environment (incl. workplace, classroom, houshold, gatherings) with a confirmed COVID-19 patient?" + "</td>"+
// 		 		 "<td>" + nonI + "</td>";
// 				document.getElementById("nonJ").innerHTML =
// 				 "<td>" + "Have you traveled together with a confirmed COVID-19 patient in close priximity (1 meter ot 3 feet) in any kind of conveyance" + "</td>"+
// 		 		 "<td>" + nonJ + "</td>";
// 				document.getElementById("nonL").innerHTML =
// 				 "<td>" + "Do you have severe acute respiratory infection or atypical pneumonia?" + "</td>"+
// 		 		 "<td>" + nonL + "</td>";
// 				// document.getElementById("summary").innerHTML =
// 				// "Do you have a fever more than or equal to 38C? " + nonA + "<br>" +
// 				// "Do you have a cough? " + nonB + "<br>" +
// 				// "Are you experiencing shortness of breath? " + nonC + "<br>" +
// 				// "Do you have a any other respiratory symptoms? " + nonD + "<br>" +
// 				// "Do you have a diarrhea? " + nonE + "<br>" +
// 				// "Have you traveled or are you residing in a country/area reporting local transmission of COVID-19? " + nonF + "<br>" +
// 				// "Have had any CLOSE CONTACT with a confirmed COVID-19 case? " + nonG + "<br>" +
// 				// "Have you provided direct care without proper PPE to confirmed COVID-19 patient? " + nonH + "<br>" +
// 				// "Have stayed in the same close environment (incl. workplace, classroom, houshold, gatherings) with a confirmed COVID-19 patient? " + nonI + "<br>" +
// 				// "Have you traveled together with a confirmed COVID-19 patient in close priximity (1 meter ot 3 feet) in any kind of conveyance " + nonJ + "<br>" +
// 				// "Do you have severe acute respiratory infection or atypical pneumonia? " + nonL;
// 				document.getElementById("sum").innerHTML =
//     			"You are categorized as PUI. Please seek medical assistance immediately."
// 			}
// 			else{
// 				// // if nonL no
// 				alert("You are neither PUI nor PUM");
// 				document.getElementById("nonA").innerHTML =
// 				 "<td>" + "Do you have a fever more than or equal to 38C?" + "</td>"+
// 		 		 "<td>" + nonA + "</td>";
// 				document.getElementById("nonB").innerHTML =
// 				 "<td>" + "Do you have a cough?" + "</td>"+
// 		 		 "<td>" + nonB + "</td>";
// 				document.getElementById("nonC").innerHTML =
// 				 "<td>" + "Are you experiencing shortness of breath?" + "</td>"+
// 		 		 "<td>" + nonC + "</td>";
// 				document.getElementById("nonD").innerHTML =
// 				 "<td>" + "Do you have a any other respiratory symptoms?" + "</td>"+
// 		 		 "<td>" + nonD + "</td>";
// 				document.getElementById("nonE").innerHTML =
// 				 "<td>" + "Do you have a diarrhea?" + "</td>"+
// 		 		 "<td>" + nonE + "</td>";
// 				document.getElementById("nonF").innerHTML =
// 				 "<td>" + "Have you traveled or are you residing in a country/area reporting local transmission of COVID-19?" + "</td>"+
// 		 		 "<td>" + nonF + "</td>";
// 				document.getElementById("nonG").innerHTML =
// 				 "<td>" + "Have had any CLOSE CONTACT with a confirmed COVID-19 case?" + "</td>"+
// 		 		 "<td>" + nonG + "</td>";
// 				document.getElementById("nonH").innerHTML =
// 				 "<td>" + "Have you provided direct care without proper PPE to confirmed COVID-19 patient?" + "</td>"+
// 		 		 "<td>" + nonH + "</td>";
// 				document.getElementById("nonI").innerHTML =
// 				 "<td>" + "Have stayed in the same close environment (incl. workplace, classroom, houshold, gatherings) with a confirmed COVID-19 patient?" + "</td>"+
// 		 		 "<td>" + nonI + "</td>";
// 				document.getElementById("nonJ").innerHTML =
// 				 "<td>" + "Have you traveled together with a confirmed COVID-19 patient in close priximity (1 meter ot 3 feet) in any kind of conveyance" + "</td>"+
// 		 		 "<td>" + nonJ + "</td>";
// 				document.getElementById("nonL").innerHTML =
// 				 "<td>" + "Do you have severe acute respiratory infection or atypical pneumonia?" + "</td>"+
// 		 		 "<td>" + nonL + "</td>";
// 				// document.getElementById("summary").innerHTML =
// 				// "Do you have a fever more than or equal to 38C? " + nonA + "<br>" +
// 				// "Do you have a cough? " + nonB + "<br>" +
// 				// "Are you experiencing shortness of breath? " + nonC + "<br>" +
// 				// "Do you have a any other respiratory symptoms? " + nonD + "<br>" +
// 				// "Do you have a diarrhea? " + nonE + "<br>" +
// 				// "Have you traveled or are you residing in a country/area reporting local transmission of COVID-19? " + nonF + "<br>" +
// 				// "Have had any CLOSE CONTACT with a confirmed COVID-19 case? " + nonG + "<br>" +
// 				// "Have you provided direct care without proper PPE to confirmed COVID-19 patient? " + nonH + "<br>" +
// 				// "Have stayed in the same close environment (incl. workplace, classroom, houshold, gatherings) with a confirmed COVID-19 patient? " + nonI + "<br>" +
// 				// "Have you traveled together with a confirmed COVID-19 patient in close priximity (1 meter ot 3 feet) in any kind of conveyance " + nonJ + "<br>" +
// 				// "Do you have severe acute respiratory infection or atypical pneumonia? " + nonL;
// 				document.getElementById("sum").innerHTML =
//     			"You are neither PUI or PUM. Stay healthy. Observe Social Distancing"
// 			}
// 		}

// 		}


// }


// let decide = false
// while (decide == false){
// 		trav = prompt('Have you travelled outside the country in the past 6 months?');
// 		trav = trav.toLowerCase();
// 		trav = trav.trim();
// 	if (trav == 'yes' || trav == 'no' ) {
		
// 		decide = true
// 		if (trav == 'yes') {
// 			checkTravellerExposure();
// 		}
// 		else {
// 			nonTrav();
// 			decide = true;
// 		}

		
// 	}
// 	else{
// 		errinput();
// 	}
// }


// end of travelers script


